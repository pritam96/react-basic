var {Router,Route,Link} = ReactRouter

var NavComponent = React.createClass({
  render: function() {
    return (

<div>  
   <nav className="navbar navbar-default navbar-static-top">
    <ul className="nav nav-pills">
      {/* Check the Css section for the selector */}
      <li><Link to="/partA" activeClassName="active">Part A</Link></li>
      <li><Link to="/partB" activeClassName="active">Part B</Link></li>
      <li><Link to="/partC" activeClassName="active">Part C</Link></li>
      <li><Link to="/partD" activeClassName="active">Part D</Link></li>
    </ul>
  </nav>
        {this.props.children}
  </div>    
      
    );
  }
});

var PartAComponent = React.createClass({
  render: function() {
    return (
      <div>
      <h2>Part A</h2>
      </div>
    );
  }
});


var PartBComponent = React.createClass({
  render: function() {
    return (
      <div>
      <h2>Part B</h2>
      </div>
    );
  }
});

var PartCComponent = React.createClass({
  render: function() {
    return (
      <div>
      <h2>Part C</h2>
      </div>
    );
  }
});


var PartDComponent = React.createClass({
  render: function() {
    return (
      <div>
      <h2>Part D</h2>
      </div>
    );
  }
});


ReactDOM.render(
  <Router>
    {/* Notice there is a NavComponent surrounding the part Components*/}
    <Route path="/" component={NavComponent}>
    <Route path ="/partA" component={PartAComponent}/>
    <Route path ="/partB" component={PartBComponent}/>
    <Route path ="/partC" component={PartCComponent}/>
    <Route path ="/partD" component={PartDComponent}/>    
    </Route>

  </Router>,
  document.getElementById('App')
);