import React, { Component } from 'react'
import { BrowserRouter} from "react-router-dom";
import Hello from './Hello/hello'
import './css/style.css'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div>
      <Hello/>
      </div>
      </BrowserRouter>
    )
  }
}

